<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
/**
 * Группа маршрутов для панели администратора
 */
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth'] ], function () {
    Route::resource('/polyathlon', 'PolyathlonController', ['as' => 'admin']);
    Route::resource('/specialty', 'SpecialtyController', ['as' => 'admin']);
    Route::resource('/group', 'GroupController', ['as' => 'admin']);
    Route::resource('/teacher', 'TeacherController', ['as' => 'admin']);
    Route::resource('/discipline', 'DisciplineController', ['as' => 'admin']);
    Route::resource('/classroom', 'ClassroomController', ['as' => 'admin']);
    Route::resource('/lesson', 'LessonController', ['as' => 'admin']);
    Route::resource('/replacement', 'ReplacementController', ['as' => 'admin']);
});

Auth::routes();
