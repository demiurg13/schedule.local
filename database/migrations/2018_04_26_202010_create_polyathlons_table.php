<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolyathlonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polyathlons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('score')->comment('Количество очков');
            $table->string('result')->comment('Результат');
            $table->string('type')->comment('Вид соревнования');
            $table->string('gender')->comment('Пол участника');
            $table->string('age_group')->comment('Возрастная группа');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polyathlons');
    }
}
