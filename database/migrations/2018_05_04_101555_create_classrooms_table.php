<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->comment('Номер аудитории');
            $table->string('corpus')->nullable()->comment('Номер учебного корпуса');
            $table->boolean('projector')->nullable()->comment('Наличие проектора в аудитории');
            $table->boolean('screen')->nullable()->comment('Наличие экрана в аудитории');
            $table->text('notice')->nullable()->comment('Дополнительная информация об аудитории');
            $table->integer('places')->nullable()->comment('Количество посадочных мест в аудитории');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms');
    }
}
