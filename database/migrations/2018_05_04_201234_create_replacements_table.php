<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replacements', function (Blueprint $table) {
            $table->increments('id');
            $table->date('day')->comment('День замены');
            $table->string('weekday')->comment('День недели');
            $table->string('group')->comment('Номер группы');
            $table->string('number')->comment('Номер занятия');
            $table->string('teacher')->comment('Преподаватель');
            $table->string('discipline')->comment('Дисциплина');
            $table->string('classroom')->comment('Аудитория');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replacements');
    }
}
