<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('weekday')->comment('День недели');
            $table->string('number')->comment('Номер занятия');
            $table->string('group')->comment('Номер группы');
            $table->string('teacher')->comment('Преподаватель');
            $table->string('discipline')->comment('Дисциплина');
            $table->string('classroom')->comment('Аудитория');
            $table->string('subgroup')->nullable()->comment('Подгруппа');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
