<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PolyathlonsTableSeeder::class);
        $this->call(SpecialtiesTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(TeachersTableSeeder::class);
        $this->call(DisciplinesTableSeeder::class);
        $this->call(ClassroomsTableSeeder::class);
        $this->call(LessonsTableSeeder::class);
        $this->call(ReplacementsTableSeeder::class);
    }
}
