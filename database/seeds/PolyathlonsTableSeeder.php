<?php

use Illuminate\Database\Seeder;

class PolyathlonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 10;
        for ($i=1; $i <= $count; $i++) {
            DB::table('polyathlons')->insert(
                [
                    'score' => $i,
                    'result' => $count - $i,
                    'type' => 'run',
                    'gender' => 'm',
                    'age_group' => 1,
                ]
            );
        }
    }
}
