<?php

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = \App\Group::orderBy('specialty')->orderBy('course')->get();
        foreach ($groups as $key => $group) {
            $specialty = \App\Specialty::where('name', $group->specialty)->first();
            $tempGroups[] = $group->course . " " . $specialty->abbreviation;
        }
        $disciplines = data_get(\App\Discipline::select('abbreviation')->get(), '*.abbreviation');
        $classroom = data_get(\App\Classroom::select('number')->get(), '*.number');
        $teachers = \App\Teacher::select('surname', 'name', 'patronymic')->get()->toArray();
        foreach ($tempGroups as $key => $group) {
            for ($weekday=1; $weekday <= 6; $weekday++) {
                for ($number=1; $number <= 8; $number++) {
                    if (($weekday==1 && $number>=7) ||($weekday==6 && $number>=5)) {
                        continue;
                    }
                    $teacher = $teachers[array_rand($teachers)];
                    $teacher = $teacher['surname'] ." ". $teacher['name'][0] .".". $teacher['patronymic'][0];
                    DB::table('lessons')->insert(
                        [
                            'weekday' => $weekday,
                            'number' => $number,
                            'group' => $group,
                            'teacher' => $teacher,
                            'discipline' => $disciplines[array_rand($disciplines)],
                            'classroom' =>  $classroom[array_rand($classroom)],
                        ]
                    );
                }
            }
        }

    }
}
