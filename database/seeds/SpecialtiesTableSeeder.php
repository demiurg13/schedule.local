<?php

use Illuminate\Database\Seeder;

class SpecialtiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Specialty::class, 3)->create();
        factory(App\Specialty::class, 1)->states('nk')->create();
        factory(App\Specialty::class, 1)->states('do')->create();
        factory(App\Specialty::class, 1)->states('f')->create();
        factory(App\Specialty::class, 1)->states('am')->create();
        factory(App\Specialty::class, 1)->states('pin')->create();
    }
}
