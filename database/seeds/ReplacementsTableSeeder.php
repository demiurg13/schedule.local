<?php

use Illuminate\Database\Seeder;

class ReplacementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Replacement::class, 5)->create();
    }
}
