<?php

use Faker\Generator as Faker;

$factory->define(App\Discipline::class, function (Faker $faker) {
    $discipline = $faker->text(20);
    $abbreviation =  strtoupper(mb_strtolower(substr($discipline, 0, 4)));
    return [
        'name' =>$discipline,
        'abbreviation' => $abbreviation,
    ];
});
