<?php

use Faker\Generator as Faker;

$factory->define(App\Classroom::class, function (Faker $faker) {
    return [
        'number' => $faker->unique()->numberBetween(1, 40),
        'corpus' => 1,
        'projector' => false,
        'screen' => false,
        'notice' => $faker->text(20),
        'places' => $faker->numberBetween(20, 30),
    ];
});
