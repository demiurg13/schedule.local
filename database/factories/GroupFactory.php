<?php

use Faker\Generator as Faker;

$factory->define(App\Group::class, function (Faker $faker) {
    $specialties = data_get(\App\Specialty::select('name')->get(), '*.name');
    return [
        'specialty' =>$specialties[0],
        // 'specialty' => $faker->randomElement($specialties),
        'course' => $faker->unique()->numberBetween(1, 4),
        'full_time' => true,
    ];
});
