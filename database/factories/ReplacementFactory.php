<?php

use Faker\Generator as Faker;

$factory->define(App\Replacement::class, function (Faker $faker) {
    $groups = \App\Group::orderBy('specialty')->orderBy('course')->get();
    foreach ($groups as $key => $group) {
        $specialty = \App\Specialty::where('name', $group->specialty)->first();
        $tempGroups[] = $group->course . " " . $specialty->abbreviation;
    }
    $disciplines = data_get(\App\Discipline::select('abbreviation')->get(), '*.abbreviation');
    $classroom = data_get(\App\Classroom::select('number')->get(), '*.number');
    $teachers = \App\Teacher::select('surname', 'name', 'patronymic')->get()->toArray();
    $teacher = $teachers[array_rand($teachers)];
    $teacher = $teacher['surname'] ." ". $teacher['name'][0] .".". $teacher['patronymic'][0];
    return [
        'day' => now(),
        'weekday'=> $faker->numberBetween(1, 6),
        'group' => $faker->randomElement($tempGroups),
        'number' => $faker->numberBetween(1, 6),
        'teacher' => $teacher,
        'discipline' => $disciplines[array_rand($disciplines)],
        'classroom' => $classroom[array_rand($classroom)],
    ];
});
