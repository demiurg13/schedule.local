<?php

use Faker\Generator as Faker;

$factory->define(App\Lesson::class, function (Faker $faker) {
    return [
        'weekday' => $faker->unique()->numberBetween(1, 7),
        'number' => $faker->unique()->numberBetween(1, 8),
        'group' => '3 пин',
        'teacher' => $faker->text(12),
        'discipline' => $faker->text(12),
        'classroom' => $faker->unique()->numberBetween(1, 7),
    ];
});
