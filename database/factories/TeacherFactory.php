<?php

use Faker\Generator as Faker;

$factory->define(App\Teacher::class, function (Faker $faker) {
    return [
        'surname' => $faker->firstName,
        'name' => $faker->lastName,
        'patronymic' => $faker->lastName,
    ];
});
