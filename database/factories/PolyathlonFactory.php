<?php

use Faker\Generator as Faker;

$factory->define(App\Polyathlon::class, function (Faker $faker) {
    return [
        'score',
        'result',
        'type',
        'gender',
        'age_group',
    ];
});
