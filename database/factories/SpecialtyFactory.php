<?php

use Faker\Generator as Faker;

$factory->define(App\Specialty::class, function (Faker $faker) {
    $name = $faker->text(20);
    $abbreviation =  mb_strtolower(substr($name, 0, 3));
    return [
        'number' => $faker->numerify('##.##.##'),
        'name' => $name,
        'abbreviation' => $abbreviation,
    ];
});

$factory->state(App\Specialty::class, 'nk', [
    'number' => '44.02.02',
    'name' => 'Преподавание в начальных классах',
    'abbreviation' => 'нк',
]);

$factory->state(App\Specialty::class, 'f', [
    'number' => '49.02.01',
    'name' => 'Физическая культура',
    'abbreviation' => 'ф',
]);

$factory->state(App\Specialty::class, 'pin', [
    'number' => '09.02.05',
    'name' => 'Прикладная информатика (по отраслям)',
    'abbreviation' => 'пин',
]);

$factory->state(App\Specialty::class, 'do', [
    'number' => '44.02.01',
    'name' => 'Дошкольное образование',
    'abbreviation' => 'до',
]);

$factory->state(App\Specialty::class, 'am', [
    'number' => '23.01.03',
    'name' => 'Автомеханик',
    'abbreviation' => 'ам',
]);
