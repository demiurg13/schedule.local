@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.groups.partials.header')

    <h3>Список групп</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Специальность</th>
                    <th>Курс</th>
                    <th>Очное/Заочное</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($groups as $key => $group)
                <tr>
                    <td>{{$group->specialty}}</td>
                    <td>{{$group->course}}</td>
                    <td>{{$group->full_time}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.group.destroy', $group)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.group.edit', $group)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
