@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="specialty">Специальность</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm form-control form-control-sm-sm" name="specialty" id="specialty" value="@if(old('specialty')){{old('specialty')}}@else{{$group->specialty or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="course">Курс</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="course" id="course" value="@if(old('course')){{old('course')}}@else{{$group->course or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="full_time">Очное/заочное обучение</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="full_time" id="full_time" value="@if(old('full_time')){{old('full_time')}}@else{{$group->full_time or ""}}@endif">
  </div>
</div>

<hr />
<div class="text-right">
    <input class="btn btn-sm btn-primary" type="submit" value="Сохранить">
    <a class="btn btn-sm btn-outline-danger" href="{{ route('admin.group.index') }}">Отмена</a>
</div>
