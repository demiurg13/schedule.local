@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.groups.partials.header')
    <h3>Редактировать группу</h3>
    <div>
        <form action="{{route('admin.group.update', $group)}}" method="post">
            {{ method_field('put')}}
            {{ csrf_field() }}
            @include('admin.groups.partials.form')
        </form>
    </div>
</main>

@endsection
