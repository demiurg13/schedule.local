@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.teachers.partials.header')
    <h3>Добавить преподавателя</h3>
    <div>
        <form action="{{route('admin.teacher.store')}}" method="post">
            {{ csrf_field() }}
            @include('admin.teachers.partials.form')
        </form>
    </div>
</main>

@endsection
