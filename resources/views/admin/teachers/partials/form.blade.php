@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="surname">Фамилия</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="surname" id="surname" value="@if(old('surname')){{old('surname')}}@else{{$teacher->surname or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="name">Имя</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="name" id="name" value="@if(old('name')){{old('name')}}@else{{$teacher->name or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="patronymic">Отчество</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="patronymic" id="patronymic" value="@if(old('patronymic')){{old('patronymic')}}@else{{$teacher->patronymic or ""}}@endif">
  </div>
</div>

<hr />
<div class="text-right">
    <input class="btn btn-sm btn-primary" type="submit" value="Сохранить">
    <a class="btn btn-sm btn-outline-danger" href="{{ route('admin.teacher.index') }}">Отмена</a>
</div>
