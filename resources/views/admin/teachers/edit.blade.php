@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.teachers.partials.header')
    <h3>Редактировать преподавателя</h3>
    <div>
        <form action="{{route('admin.teacher.update', $teacher)}}" method="post">
            {{ method_field('put')}}
            {{ csrf_field() }}
            @include('admin.teachers.partials.form')
        </form>
    </div>
</main>

@endsection
