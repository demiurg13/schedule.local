@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.replacements.partials.header')

    <h3>Список</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Дата</th>
                    <th>День недели</th>
                    <th>Номер урока</th>
                    <th>Группа</th>
                    <th>Преподаватель</th>
                    <th>Дисциплина</th>
                    <th>Аудитория</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($replacements as $key => $replacement)
                <tr>
                    <td>{{$replacement->day}}</td>
                    <td>{{$replacement->weekday}}</td>
                    <td>{{$replacement->number}}</td>
                    <td>{{$replacement->group}}</td>
                    <td>{{$replacement->teacher}}</td>
                    <td>{{$replacement->discipline}}</td>
                    <td>{{$replacement->classroom}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.replacement.destroy', $replacement)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.replacement.edit', $replacement)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
