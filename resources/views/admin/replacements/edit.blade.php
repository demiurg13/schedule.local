@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.replacements.partials.header')
    <h3>Редактировать</h3>
    <div>
        <form action="{{route('admin.replacement.update', $replacement)}}" method="post">
            {{ method_field('put')}}
            {{ csrf_field() }}
            @include('admin.replacements.partials.form')
        </form>
    </div>
</main>

@endsection
