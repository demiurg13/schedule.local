@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="weekday">День недели</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="weekday" id="weekday" value="@if(old('weekday')){{old('weekday')}}@else{{$lesson->weekday or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="number">Номер урока</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="number" id="number" value="@if(old('number')){{old('number')}}@else{{$lesson->number or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="group">Группа</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="group" id="group" value="@if(old('group')){{old('group')}}@else{{$lesson->group or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="teacher">Преподаватель</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="teacher" id="teacher" value="@if(old('teacher')){{old('teacher')}}@else{{$lesson->teacher or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="discipline">Дисциплина</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="discipline" id="discipline" value="@if(old('discipline')){{old('discipline')}}@else{{$lesson->discipline or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="classroom">Аудитория</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="classroom" id="classroom" value="@if(old('classroom')){{old('classroom')}}@else{{$lesson->classroom or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="subgroup">Подгруппа</label>
  <div class="col-sm-10">
      <input type="text" class="form-control" name="subgroup" id="subgroup" value="@if(old('subgroup')){{old('subgroup')}}@else{{$lesson->subgroup or ""}}@endif">
  </div>
</div>

<hr />
<div class="text-right">
    <input class="btn btn-sm btn-primary" type="submit" value="Сохранить">
    <a class="btn btn-sm btn-outline-danger" href="{{ route('admin.lesson.index') }}">Отмена</a>
</div>
