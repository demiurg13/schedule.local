@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.lessons.partials.header')

    <h3>Список</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>День недели</th>
                    <th>Номер урока</th>
                    <th>Группа</th>
                    <th>Преподаватель</th>
                    <th>Дисциплина</th>
                    <th>Аудитория</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($lessons as $key => $lesson)
                <tr>
                    <td>{{$lesson->weekday}}</td>
                    <td>{{$lesson->number}}</td>
                    <td>{{$lesson->group}}</td>
                    <td>{{$lesson->teacher}}</td>
                    <td>{{$lesson->discipline}}</td>
                    <td>{{$lesson->classroom}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.lesson.destroy', $lesson)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.lesson.edit', $lesson)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
