@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.lessons.partials.header')
    <h3>Добавить урок</h3>
    <div>
        <form action="{{route('admin.lesson.store')}}" method="post">
            {{ csrf_field() }}
            @include('admin.lessons.partials.form')
        </form>
    </div>
</main>

@endsection
