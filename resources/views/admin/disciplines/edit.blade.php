@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.disciplines.partials.header')
    <h3>Редактировать дисциплину</h3>
    <div>
        <form action="{{route('admin.discipline.update', $discipline)}}" method="post">
            {{ method_field('put')}}
            {{ csrf_field() }}
            @include('admin.disciplines.partials.form')
        </form>
    </div>
</main>

@endsection
