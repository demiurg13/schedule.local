@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.disciplines.partials.header')

    <h3>Список дисциплин</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Название дисциплины</th>
                    <th>Краткое название дисциплины</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($disciplines as $key => $discipline)
                <tr>
                    <td>{{$discipline->name}}</td>
                    <td>{{$discipline->abbreviation}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.discipline.destroy', $discipline)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.discipline.edit', $discipline)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
