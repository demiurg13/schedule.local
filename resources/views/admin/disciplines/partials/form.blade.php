@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="name">Дисциплина</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="name" id="name" value="@if(old('name')){{old('name')}}@else{{$discipline->name or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="abbreviation">Краткое название дисциплины</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="abbreviation" id="abbreviation" value="@if(old('abbreviation')){{old('abbreviation')}}@else{{$discipline->abbreviation or ""}}@endif">
  </div>
</div>

<hr />
<div class="text-right">
    <input class="btn btn-sm btn-primary" type="submit" value="Сохранить">
    <a class="btn btn-sm btn-outline-danger" href="{{ route('admin.discipline.index') }}">Отмена</a>
</div>
