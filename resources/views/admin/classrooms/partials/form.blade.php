@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="number">Номер аудитории</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="number" id="number" value="@if(old('number')){{old('number')}}@else{{$classroom->number or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="corpus">Корпус</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="corpus" id="corpus" value="@if(old('corpus')){{old('corpus')}}@else{{$classroom->corpus or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="projector">Наличие проектора</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="projector" id="projector" value="@if(old('projector')){{old('projector')}}@else{{$classroom->projector or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="screen">Наличие экрана</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="screen" id="screen" value="@if(old('screen')){{old('screen')}}@else{{$classroom->screen or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="places">Количество мест</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="places" id="places" value="@if(old('places')){{old('places')}}@else{{$classroom->places or ""}}@endif">
  </div>
</div>

<div class="form-group row">
  <label class="col-sm-2 col-form-label" for="notice">Примечание</label>
  <div class="col-sm-10">
      <input type="text" class="form-control form-control-sm" name="notice" id="notice" value="@if(old('notice')){{old('notice')}}@else{{$classroom->notice or ""}}@endif">
  </div>
</div>

<hr />
<div class="text-right">
    <input class="btn btn-sm btn-primary" type="submit" value="Сохранить">
    <a class="btn btn-sm btn-outline-danger" href="{{ route('admin.classroom.index') }}">Отмена</a>
</div>
