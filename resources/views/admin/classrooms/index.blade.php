@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.classrooms.partials.header')

    <h3>Список аудиторий</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Номер аудитории</th>
                    <th>Корпус</th>
                    <th>Количество мест</th>
                    <th>Примечание</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($classrooms as $key => $classroom)
                <tr>
                    <td>{{$classroom->number}}</td>
                    <td>{{$classroom->corpus}}</td>
                    <td>{{$classroom->places}}</td>
                    <td>{{$classroom->notice}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.classroom.destroy', $classroom)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.classroom.edit', $classroom)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
