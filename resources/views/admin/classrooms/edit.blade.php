@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.classrooms.partials.header')
    <h3>Редактировать аудиторию</h3>
    <div>
        <form action="{{route('admin.classroom.update', $classroom)}}" method="post">
            {{ method_field('put')}}
            {{ csrf_field() }}
            @include('admin.classrooms.partials.form')
        </form>
    </div>
</main>

@endsection
