@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.polyathlons.partials.header')

    <h3>Список</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Баллы</th>
                    <th>Результат</th>
                    <th>Дисицплина</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($polyathlons as $key => $polyathlon)
                <tr>
                    <td>{{$polyathlon->score}}</td>
                    <td>{{$polyathlon->result}}</td>
                    <td>{{$polyathlon->type}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.polyathlon.destroy', $polyathlon)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.polyathlon.edit', $polyathlon)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
