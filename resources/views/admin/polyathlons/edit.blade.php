@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.polyathlons.partials.header')
    <h3>Редактировать</h3>
    <div>
        <form action="{{route('admin.polyathlon.update', $polyathlon)}}" method="post">
            {{ method_field('put')}}
            {{ csrf_field() }}
            @include('admin.polyathlons.partials.form')
        </form>
    </div>
</main>

@endsection
