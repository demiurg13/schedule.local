@extends('layouts.admin')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.specialties.partials.header')
    <h3>Добавить специальность</h3>
    <div>
        <form action="{{route('admin.specialty.store')}}" method="post">
            {{ csrf_field() }}
            @include('admin.specialties.partials.form')
        </form>
    </div>
</main>

@endsection
