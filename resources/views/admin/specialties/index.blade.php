@extends('layouts.admin')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    @include('admin.specialties.partials.header')

    <h3>Список специальностей</h3>
    <div class="table-responive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Номер специальности</th>
                    <th>Название специальности</th>
                    <th>Аббревиатура</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($specialties as $key => $specialty)
                <tr>
                    <td>{{$specialty->number}}</td>
                    <td>{{$specialty->name}}</td>
                    <td>{{$specialty->abbreviation}}</td>
                    <td class="text-right">
                        <form action="{{route('admin.specialty.destroy', $specialty)}}" method="post">
                            {{ method_field('delete') }}
                            {{ csrf_field() }}

                            <a class="btn btn-sm btn-outline-primary" href="{{route('admin.specialty.edit', $specialty)}}">
                                <span data-feather="edit-2"></span>
                            </a>

                            <button type="submit" class="btn btn-sm btn-outline-danger">
                                <span data-feather="x"></span>
                            </button>
                        </form>
                    </td>
                </tr>
                @empty
                    <h2>Данные отстствуют</h2>
                @endforelse
            </tbody>
        </table>
    </div>
</main>
@endsection
