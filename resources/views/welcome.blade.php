@extends('layouts.user')

@section('content')
<main role="main" class="@auth col-md-10 ml-sm-auto col-lg-10 @endauth pt-3 px-4">

    {{-- {{dd($groupReplacements)}} --}}

    <div class="container">
      <div class="row">
          <div class="col-md-12">
              <div class="card bg-light">
                  <div class="card-body">
                      <h2 class="card-title">Замены на {{ $day }}</h2>
                      <table class="table table-striped table-sm table-bordered table-hover">
                          <tr>
                              <th>Группа</th>
                              <th>Урок</th>
                              <th>Предмет</th>
                              <th>Преподаватель</th>
                              <th>Аудитория</th>
                          </tr>
                          @forelse ($groupReplacements as $key => $replacement)
                              <tr>
                                  <td>{{$replacement->group}}</td>
                                  <td>{{$replacement->number}}</td>
                                  <td>{{$replacement->discipline}}</td>
                                  <td>{{$replacement->teacher}}</td>
                                  <td>{{$replacement->classroom}}</td>
                              </tr>
                          @empty
                        @endforelse
                    </table>
                  </div>
            </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">

            @foreach ($groupLessons as $group => $weekday)

                <div class="col">
                <h3 class="text-center">{{ $group }}</h3>
                @foreach ($weekday as $key => $day)

                    <table class="table table-striped table-sm table-bordered">

                    @foreach ($day as $key => $lesson)
                        @if ($loop->first)
                            <tr>
                                <th colspan="4" class="text-center">
                                    @switch($lesson->weekday)
                                        @case(1)
                                            Понедельник
                                            @break

                                        @case(2)
                                            Вторник
                                            @break

                                        @case(3)
                                            Среда
                                            @break

                                        @case(4)
                                            Четверг
                                            @break

                                        @case(5)
                                            Пятница
                                            @break

                                        @case(6)
                                            Суббота
                                            @break

                                    @endswitch
                                </th>
                            </tr>
                            <tr>
                                <td>#</td>
                                <td>Предмет</td>
                                <td>Преподаватель</td>
                                <td>Ауд.</td>
                            </tr>
                        @endif

                        <tr>
                            <td>{{$lesson->number}}</td>
                            <td>{{$lesson->discipline}}</td>
                            <td>{{$lesson->teacher}}</td>
                            <td>{{$lesson->classroom}}</td>
                        </tr>
                    @endforeach

                    </table>

                @endforeach

                </div>
            @endforeach

      </div>
    </div>


</main>

@endsection
