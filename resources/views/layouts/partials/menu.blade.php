<ul class="nav flex-column">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.specialty.index') }}">
          <span data-feather="chevrons-right"></span>
          Специальности
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.group.index') }}">
        <span data-feather="chevrons-right"></span>
        Группы
    </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.teacher.index') }}">
        <span data-feather="chevrons-right"></span>
        Преподаватели
      </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.discipline.index') }}">
        <span data-feather="chevrons-right"></span>
        Дисциплины
      </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.lesson.index') }}">
        <span data-feather="chevrons-right"></span>
        Уроки
      </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.classroom.index') }}">
        <span data-feather="chevrons-right"></span>
        Аудитории
      </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.replacement.index') }}">
        <span data-feather="chevrons-right"></span>
        Замены
      </a>
    </li>
</ul>
