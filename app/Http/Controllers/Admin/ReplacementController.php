<?php

namespace App\Http\Controllers\Admin;

use App\Replacement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReplacementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $replacements = Replacement::all();
        return view('admin.replacements.index', [
            'replacements' => $replacements,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.replacements.create', [
            'replacement' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Replacement::create($request->all());
        return redirect()->route('admin.replacement.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function show(Replacement $replacement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function edit(Replacement $replacement)
    {
        return view('admin.replacements.edit', [
            'replacement' => $replacement,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Replacement $replacement)
    {
        $replacement->update($request->all());
        return redirect()->route('admin.replacement.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Replacement  $replacement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Replacement $replacement)
    {
        $replacement->delete();
        return redirect()->route('admin.replacement.index');
    }
}
