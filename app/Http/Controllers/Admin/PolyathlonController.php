<?php

namespace App\Http\Controllers\Admin;

use App\Polyathlon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PolyathlonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $polyathlons = Polyathlon::all();
        return view('admin.polyathlons.index', [
            'polyathlons' => $polyathlons,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.polyathlons.create', [
            'polyathlon' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Polyathlon::create($request->all());
        return redirect()->route('admin.polyathlon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Polyathlon  $polyathlon
     * @return \Illuminate\Http\Response
     */
    public function show(Polyathlon $polyathlon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Polyathlon  $polyathlon
     * @return \Illuminate\Http\Response
     */
    public function edit(Polyathlon $polyathlon)
    {
        return view('admin.polyathlons.edit', [
            'polyathlon' => $polyathlon,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Polyathlon  $polyathlon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Polyathlon $polyathlon)
    {
        $polyathlon->update($request->all());
        return redirect()->route('admin.polyathlon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Polyathlon  $polyathlon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Polyathlon $polyathlon)
    {
        $polyathlon->delete();
        return redirect()->route('admin.polyathlon.index');
    }
}
