<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Group;
use App\Specialty;
use App\Replacement;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {

        $day = \Carbon\Carbon::now()->format('Y-m-d');
        $groups = Group::orderBy('specialty')->orderBy('course')->get();
        foreach ($groups as $key => $group) {
            $specialty = Specialty::where('name', $group->specialty)->first();
            $tempGroups[] = $group->course . " " . $specialty->abbreviation;
        }
        foreach ($tempGroups as $group) {
            for ($weekday=1; $weekday <= 6; $weekday++) {
                $groupLessons[$group][$weekday] = Lesson::where('group', $group)
                                                    ->where('weekday', $weekday)
                                                    ->orderBy('number')
                                                    ->get();
            }
        }
        $groupReplacements = Replacement::where('day', $day)
                                                ->orderBy('group')
                                                ->orderBy('weekday')
                                                ->orderBy('number')
                                                ->get();
        return view('welcome', [
            'groupLessons' => $groupLessons,
            'groupReplacements' => $groupReplacements,
            'day' => $day,
        ]);
    }
}
