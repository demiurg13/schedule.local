<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replacement extends Model
{
    /**
     * Атрибуты, которые можно использовать массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'day',
        'weekday',
        'group',
        'number',
        'teacher',
        'discipline',
        'classroom',
    ];
}
