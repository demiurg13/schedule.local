<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polyathlon extends Model
{
    /**
     * Атрибуты, которые можно использовать массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'score', 'result', 'type', 'gender', 'age_group',
    ];
}
