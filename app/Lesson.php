<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    /**
     * Атрибуты, которые можно использовать массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'weekday', 'number', 'group', 'teacher', 'discipline', 'classroom', 'subgroup',
    ];
}
