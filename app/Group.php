<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * Атрибуты, которые можно использовать массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'specialty', 'course', 'full_time',
    ];
}
