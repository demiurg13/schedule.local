<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    /**
     * Атрибуты, которые можно использовать массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'corpus', 'projector', 'screen', 'notice', 'places',
    ];
}
